## v4.0.0 (22.08.2022)

- *added*
  - new option `-rmo|--read-mysql-optionfiles` to read other MySQL option files (see [comment in issue #27](https://codeberg.org/BernieO/calcardbackup/issues/27#issuecomment-587596))
- *improved*
  - minor code optimizations
  - minor corrections/enhancements in comments

## v3.5.0 (23.05.2022)

- *verified*
  - compatible with ownCloud 10.10

## v3.4.2 (30.04.2022)

- *improved*
  - minor improvements in both README files
- *verified*
  - compatible with Nextcloud 24.0

## v3.4.1 (22.12.2021)

- *verified*
  - compatible with ownCloud 10.9

## v3.4.0 (01.12.2021)

- *fixed*
  - cast fields to CHAR in two database queries (related to issue #40)
  - quote a variable to prevent globbing
- *verified*
  - compatible with Nextcloud 23.0

## v3.3.0 (09.11.2021)

- *fixed*
  - respect custom database names for SQLite3

## v3.2.0 (10.10.2021)

- *fixed*
  - casting deleted_at as CHAR solves a problem with PostgreSQL (issue #40)

## v3.1.0 (25.07.2021)

- *added*
  - link to *calcardbackup* in the archLinux User Package Repository in both READMEs
- *verified*
  - compatible with ownCloud 10.8

## v3.0.0 (01.07.2021)

#### Important changes in this release:

:dizzy: Nextcloud 22 introduces a caldav trashbin for deleted calendars/components. If *calcardbackup* finds deleted calendars/components in the database, they will also be backed up.

- *added*
  - also backup deleted calendars and deleted components from the calendar trashbin for Nextcloud >= 22
  - add section *File Naming Convention* to both READMEs
- *changed*
  - naming scheme for shared items is changed from `displayname_shared_by_username` to `displayname_shared-by-username`
- *fixed*
  - do not overwrite existing files when using option `-one|--one-file-per-component`
- *improved*
  - minor code optimization when checking server version
- *verified*
  - compatible with Nextcloud 22.0

## v2.1.1 (27.05.2021)

- *improved*
  - both READMEs now contain a section with detailed instructions on how to migrate from GitHub to Codeberg

## v2.1.0 (16.05.2021)

- *fixed*
  - tolerate `config.php` not ending with a line break (issue #37)
  - delete traces of removed option `-g|--get-via-http` from `calcardbackup.conf.example`
- *improved*
  - in case of an error *calcardbackup* prints the last executed command

## v2.0.0 (05.05.2021)

#### Important changes in this release:

:tada: With the help of the included PHP script `calcardbackup_wrapper.php` there is now a good chance to be able [to run *calcardbackup* even without shell access](README.md#can-i-use-this-without-shell-access-to-the-server)

:checkered_flag: *calcardbackup* has been adapted to use temporary files and kernel caching instead of Bashs' file descriptors. Thus the memory consumption is only fraction of what it used to be. These measures give a massive performance boost and make the script run two to three times faster! :zap:

:warning: Option `-g|--get-via-http` is removed with this release (this option was deprecated since `v0.9.4`, 27.08.2019).

- *added*
  - wrapper script `calcardbackup_wrapper.php` to run calcardbackup without shell access
  - add sections [Get in touch](README.md#get-in-touch), [Donations](README.md#donations) and [License](README.md#license) to README files
- *changed*
  - convert `changelog` to markdown to increase readability (`changelog` -> `changelog.md`)
  - use `stable` as default branch
- *deprecated*
  - branch `master` is deprecated
- *fixed*
  - don't overwrite files, if a user has items with identical displaynames
- *improved*
  - update links in both README files to point to a more recent version of Nextclouds `config.php`
  - move creation of filenames for backup files to separate function
  - detect failing file/folder deletion and print info to user
  - use temp files and kernel caching instead of file descriptors
  - deduplicate some code regarding IFS
  - minor changes in code and comments to improve readability
  - use filedescriptor for mysql options only when available
- *removed*
  - option `-g|--get-via-http` (deprecated since `v0.9.4`, 27.08.2019)
- *verified*
  - compatible with ownCloud 10.7

## v1.9.0 (21.02.2021)

- *added*
  - brief note about unescaped percent signs (`%`) in crontab files (issue #33)
- *verified*
  - compatible with Nextcloud 21.0
  - compatible with ownCloud 10.6

## v1.8.1 (29.09.2020)

- *fixed*
  - remove accidentally added blank first line

## v1.8.0 (25.09.2020)

- *verified*
  - compatible with Nextcloud 20.0
  - compatible with ownCloud 10.5

## v1.7.0 (20.06.2020)

- *added*
  - section for syncloud users to `README.md` and `README_GER.md`
- *verified*
  - compatible with Nextcloud 19.0
  - compatible with ownCloud 10.4

## v1.6.0 (03.06.2020)

- *added*
  - support for snap installations with PostgreSQL

## v1.5.0 (27.04.2020)

- *fixed*
  - use preconfigured values for mysql when using snap (pull-request #29)
- *improved*
  - minor code optimization for MySQL/MariaDB

## v1.4.0 (13.04.2020)

- *fixed*
  - some links were broken in content directory of both README files
  - catch error, if connection to database can't be established (issue #27)
  - always use relative path when checking for package `tar` (not part of, but noticed in issue #27)
- *improved*
  - use `--defaults-file` instead of `--defaults-extra-file` for mysql (issue #27)
  - set default-character-set for MySQL/MariaDB
  - replace tilde at beginning of given paths with `${HOME}`

## v1.3.0 (05.04.2020)

- *changed*
  - calcardbackup now uses `tar.gz`, if `zip` is not available (instead of exiting)
- *improved*
  - minor code optimizations
  - use bash builtins to check for empty backup directory
  - simplify checks for existing tables
  - unify code for MySQL and MySQL in a snap installation
  - checks for packages are now failsafe

## v1.2.0 (20.02.2020)

- *fixed*
  - also backup `VAVAILABILITY` components when using option `-one`
  - redirect output properly when checking for packages
- *improved*
  - minor modifications in `README.md` and `README_GER.md`
  - remove unneeded quoting of lhs in conditional expressions

## v1.1.0 (27.01.2020)

- *added*
  - section about updating calcardbackup in `README.md` and `README_GER.md`
- *improved*
  - accelerate backup of addressbooks and calendars
  - make sure to always delete a `CR` before adding `CRLF` at the end of lines

## v1.0.0 (17.01.2020)

#### Important changes in this release:

:tada: This is the first stable release!

:house: *calcardbackup* moved to [codeberg.org](https://codeberg.org)

- *added*
  - new option `-one|--one-file-per-component` to backup components to separate files (issue #19)
- *improved*
  - check for vCards in legacy version 2.1 and print warning if found (issue #25)
  - better coding style of some commands
  - some minor corrections of texts and inline comments
  - updated `README.md` and `README_GER.md`
- *verified*
  - compatible with Nextcloud 18.0

## v0.9.6 (07.01.2020)

__NOTICE:__ this is the last release hosted on GitHub

- *added*
  - section for Synology DSM users to both READMEs (issue #24)
  - print info about moving to [codeberg.org](https://codeberg.org)

## v0.9.5 (25.09.2019)

- *fixed*
  - adapt check of `status.php` for new parameter `extendedSupport` in Nextcloud 17.0
- *verified*
  - compatible with Nextcloud 17.0

## v0.9.4 (27.08.2019)

#### Important changes in this release:

:warning: Option `-g|--get-via-http` is deprecated and might be removed in the future.

- *added*
  - print notice and add note to `README.md` and `README_GER.md` that option `-g|--get-via-http` is deprecated and might be removed in the future
  - add section about cronjob to `README.md` and `README_GER.md` (issue #22)
- *fixed*
  - ignore cached objects of webcal calendars
- *improved*
  - be more verbose, if command line client for according database is not installed
  - minor corrections in `README.md` and `README_GER.md`

## v0.9.3 (24.06.2019)

- *improved*
  - increased portability by using `printf` instead of `echo`
  - various performance improvements

## v0.9.2 (11.06.2019)

- *fixed*
  - eliminate minor error introduced with `v0.9.1` when checking for group shares with SQLite3

## v0.9.1 (08.06.2019)

- *fixed*
  - calendarsubscriptions of the last given username were not backed up, when using option `-u`
- *improved*
  - some redundant code has been removed to increase code readability
  - use absolute path to sqlite3 database to simplify code
  - do not keep arrays longer than needed in memory

## v0.9.0 (03.06.2019)

- *fixed*
  - eliminated error due to pathname expansion when using option `-ltm` or `-r` (issue #21)
  - use current timestamp to determine backups to be deleted with `-ltm` or `-r` (issue #21)
  - eliminate error on non GNU-systems, if backup folder needs to be created
- *improved*
  - minor addition to both READMEs about extracting from a broken ownCloud <= `8.2`
  - treat server as >= `9.0`, if `version` is missing in `config.php`
  - *calcardbackup* now runs on various systems (Linux, *BSD, SunOS, Darwin, Minix)

## v0.8.10 (25.04.2019)

- *added*
  - print warning when using option `-g|--get-via-http` to encourage to not use it anymore
- *improved*
  - minor text corrections
  - only check compression method, if backup shall be compressed
- *verified*
  - compatible with Nextcloud 16.0

## v0.8.9 (10.04.2019)

- *added*
  - support for CSS3 color names according to [RFC 7986](https://tools.ietf.org/html/rfc7986)
- *fixed*
  - don't exit when running with deprecated option `-g`, if `version` is hidden (ownCloud)
  - don't print header when running in batch mode with option `-b|--batch`
- *improved*
  - use "official" patch version from `versionstring` as version for ownCloud `5.0`
  - look for calendarsubscriptions only in servers supporting them (>= `9.0`)

## v0.8.8 (03.04.2019)

- *improved*
  - prettify output for option `-h|--help`
  - minor code optimization when file with usernames is used with option `-u|--usersfile`
- *fixed*
  - tolerate calendarcolor with more (or less) than 7 characters (issue #20)

## v0.8.7 (20.03.2019)

- *fixed*
  - don't backup system addressbook
  - don't interpret certain paths to ownCloud/Nextcloud folder falsely as an option
  - don't collect info about shares, if no calendars or addressbooks found in database
  - some corrections and additions (section with links) in `README.md` and `README_GER.md`
  - use correct name for option `-ltm` in `examples/calcardbackup.conf.example`
  - print error when using option `-g` together with a server being in maintenance mode
- *improved*
  - unset arrays when not being needed anymore to free up some memory
  - allow arguments starting with a hyphen
  - don't use given URL, if it is not among trusted domains in `config.php`
  - minor code optimizations and corrections of some comments
  - print header before errors are possibly occuring

## v0.8.6 (13.02.2019)

- *fixed*
  - take into account that principals can be `principals/<username>` from legacy installs
  - don't request unset value instead of `status.php`, if `overwrite.cli.url` is unset
  - if installed, prefer `gpg2` over `gpg` to encrypt backup (issue #18)
  - minor typo in declaring local variables
  - only use first line as calendar-/addressbookname, if displayname is multiline (issue #17)
  - minor typos in changelog for `v0.3.1` and in three comments (`version.hide`)
- *improved*
  - be more gentle and verbose, if no items found for users given in usersfile
  - complete rewrite of vendor detection
- *verified*
  - compatible with ownCloud 10.1

## v0.8.5 (28.01.2019)

- *fixed*
  - eliminate error if calendar/addressbooks-tables are not existing when using PostgreSQL
- *improved*
  - downscale and comment checks for existing tables in MySQL
  - use capital letters for keywords and functions in SQL queries

## v0.8.4 (11.12.2018)

- *fixed*
  - also identify lowercased vendorname in vendor detection
- *verified*
  - compatible with Nextcloud 15.0

## v0.8.3 (30.11.2018)

- *fixed*
  - take into account that `overwrite.cli.url` may contain a trailing slash
  - set calendarcolor also for shared calendars

## v0.8.2 (20.11.2018)

- *added*
  - german version of `README.md` (`README_GER.md`)

## v0.8.1 (10.11.2018)

- *added*
  - new option `-ltm|--like-time-machine` to keep backups similar to apples time machine
  - also backup URLs of subscribed calendars
- *fixed*
  - distinguish between unset and empty parameters in `config.php` (issue #16)
  - use default values for unset parameters in `config.php`
  - don't read table `[PREFIX]calendarsubscriptions`, if it does not exist in database
  - minor corrections in `README.md`

## v0.8.0 (30.10.2018)

#### Important changes in this release

:tada: Starting with version `v0.8.0`, there is no need anymore for a file with user credentials because as default all data is fetched directly from the database.  
If only calendars/addressbooks of certain users shall be backed up, list them in users.txt without any passwords.

:warning: All users upgrading calcardbackup from a previous version to version `v0.8.0` or above are strongly advised to delete the file with users credentials - or at least to remove the cleartext passwords from this file!

- *added*
  - new option `-g|--get-via-http` for deprecated behaviour to get files via http request
  - support environment variables `OWNCLOUD_CONFIG_DIR` and `NEXTCLOUD_CONFIG_DIR` (issue #9)
- *changed*
  - option `-f|--fetch-from-database` is now the default (thus it has no function anymore)
  - don't look for file `users.txt` in scripts directory, if not given via option `-u`
- *fixed*
  - also pass `--host` assignment to psql, if `dbhost` contains neither socket nor port (issue #10)
- *improved*
  - error and exit only when using option `-g`, if Server is offline
  - `cURL` is not a requirement anymore unless using deprecated option `-g`
  - error and exit only when using option `-g`, if Nextcloud/ownCloud need to upgrade the database
  - downscale of checks for existing tables in PostgreSQL
  - all error messages take into account if script is run with options or config file
  - use `#!/usr/bin/env bash` as shebang and `readlink` without verbosity to increase interoperability with non GNU systems (issues #13, #14)

## v0.7.2 (19.09.2018)

- *fixed*
  - don't error and exit, if there are no calendars or addressbooks existing
- *improved*
  - don't suppress error messages from PostgreSQLs interactive terminal `psql`

## v0.7.1 (13.09.2018)

- *improved*
  - various code optimizations to speed up exporting calendars with option `-f`
  - don't error and exit with option `-f`, if maintenance mode is active
- *verified*
  - compatible with Nextcloud 14.0

## v0.7.0 (03.09.2018)

#### Important changes in this release

:tada: *calcardbackup* `v0.7.0` also exports calendars by fetching data directly from the database when using option `-f|--fetch-from-database`. Thus user credentials are not needed anymore which is a big security improvement.

:warning: It is recommended to always use the new option `-f|--fetch-from-database`, which will become the default in *calcardbackup* `v0.8.0`.

- *added*
  - also backup calendars directly from database when using option `-f|--fetch-from-database`
  - if no `users.txt` is found when using option `-f`, all available items will be backed up
  - check for valid value of `include_shares` when using a config file for *calcardbackup*
- *improved*
  - passwords can be omitted in `users.txt` when using option `-f|--fetch-from-database`
  - do backup items, if `backup_calendars` or `backup_addressbooks` have an invalid value in config file

## v0.6.2 (17.08.2018)

- *fixed*
  - eliminate error, if run with option `-i|--include-shares` but no items are shared
  - correction of changelog for `v0.6.1`
- *improved*
  - only change directory, if database will be read

## v0.6.1 (15.08.2018)

- *fixed*
  - eliminate error, if run with option `-i|--include-shares` but either only calendars or addressbooks are shared

## v0.6.0 (08.08.2018)

#### Important changes in this release

:tada: The new option `-f|--fetch-from-database` exports addressbooks by fetching the data directly from the database. This needs only a fraction of the time compared to downloading them via http from Nextcloud/ownCloud. It is recommended to always use this new option.

- *added*
  - option `-f|--fetch-from-database` to backup addressbooks direct from database

## v0.5.2 (24.07.2018)

- *fixed*
  - avoid possible error in `psql` command, if no port/socket configured in `config.php`
- *improved*
  - remove unnecessary backslashes

## v0.5.1 (24.06.2018)

- *added*
  - print notice if user running the script can't read the full path to necessary files (issue #8)
- *fixed*
  - detection of hidden version number in `status.php` (only ownCloud)
  - don't use string comparison for numbers
  - use `if-then-else` to avoid missleading messages in case a function returns `false`
- *improved*
  - use `read -r` to not let bash interpret special characters

## v0.5.0 (09.06.2018)

- *added*
  - check for `nextcloud.mysql-client` when run with `-p|--snap` (or `snap="yes"`)
- *fixed*
  - some minor bugs
  - some minor text corrections
- *improved*
  - make script failsafe with `set -euo pipefail`
  - simplified handling of catching errors
  - switch to datadirectory only when using SQLite3
  - remove redundant brackets from conditional expressions
  - be more verbose by printing the invalid line when exiting because of an invalid config file

## v0.4.2 (27.05.2018)

:tada: *calcardbackup* now supports [nextcloud-snap](https://github.com/nextcloud/nextcloud-snap)

- *added*
  - option `-p|--snap` adds support for [nextcloud-snap](https://github.com/nextcloud/nextcloud-snap) (issue #6)
- *fixed*
  - quote passwords in `mysql` statement to pass passwords with special characters correctly to MySQL/MariaDB (issue #7)

## v0.4.1 (06.03.2018)

- *fixed*
  - detect vendor via http(s) instead of using filesystem (issue #5)

## v0.4.0 (06.03.2018)

#### Important changes in this release:

:tada: With this release *calcardbackup* also supports PostgreSQL databases.

- *added*
  - support PostgreSQL
- *changed*
  - use vendor (Nextcloud/ownCloud) instead of custom productname in output
- *fixed*
  - proper treatment of user-/addressbooknames containing spaces (encode spaces in url)

## v0.3.2 (13.02.2018)

- *fixed*
  - use `readlink -f` instead of `readlink -m` to create compatibility with `busybox` (issue #4)

## v0.3.1 (06.02.2018)

- *added*
  - check whether ownCloud/Nextcloud requires a database-upgrade
- *fixed*
  - take account of ownCloud 10.0 options in `config.php` (`version.hide`, `show_server_hostname`)
- *verified*
  - compatible with Nextcloud 13.0

## v0.3.0 (21.09.2017)

#### Important changes in this release:

:tada: `grep` and `sed` are  not a requirement anymore to run *calcardbackup*

- *added*
  - print notice, if chosen `compression_method` is not supported
- *fixed*
  - improve stability due to minor syntax corrections
  - correction of error message, if different versions are found at given url and given path
- *improved*
  - reduce number of database queries
  - bash-builtins replace `sed` commands: `sed` is not a requirement anymore
  - conditional expressions replace `grep` commands: `grep` is not a requirement anymore
  - increased security for mysql commands
  - revise comments to increase readability of code
  - code optimizations (use local variables, prepare database query not to early)

## v0.2.2 (12.08.2017)
- *fixed*
  - improve stability (better handling of variables)
  - support custom productname
  - items shared with groups did not get backed up (when using option `-i|--include-shares`)

## v0.2.1 (29.07.2017)

- *fixed*
  - proper treatment of value `dbhost` in `config.php` (portnumber/socket)

## v0.2.0 (30.06.2017)

#### Important changes in this release:

:tada: The new option `-i|--include-shares` creates the possibility to keep the users passwords secret.

- *added*
  - possibility to backup shared addressbooks/calendars (option `-i|--include-shares`)

## v0.1.2 (26.05.2017)

- *added*
  - print version number in regular output of script
  - possibility to backup only addressbooks or only calendars (options `-na`, `-nc`)
  - check for backed up files and print warning, if nothing was backed up
- *fixed*
  - in batch mode, backup did get compressed despite of given option `-x|--uncompressed`
  - create empty file, if addressbook contains no contacts (compatibility for `curl` >= 7.42.0)
  - replace unsafe characters in filename with underscores
  - adjust mysql commands to avoid warning messages by mysql >= 5.6
- *verified*
  - works also with recently released ownCloud 10.0 and Nextcloud 12.0

## v0.1.1 (11.05.2017)

- *added*
  - version number (for option `-h`)
- *changed*
  - use given URL, if it is different from `overwrite.cli.url`
- *fixed*
  - use `dbtableprefix`
- *improved*
  - reading value from `config.php`

## v0.1.0 (30.04.2017)

- :tada: Here is the initial release of *calcardbackup*
