<?php
/**
 * calcardbackup_wrapper.php - wrapper script to run calcardbackup without shell access
 * Copyright (C) 2021 Bernhard Ostertag
 *
 * Source: https://codeberg.org/BernieO/calcardbackup
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



/**
 * Configuration:
 * give your calcardbackup command with the path to your ownCloud/Nextcloud installation and desired options here.
 */
$command = "./calcardbackup /PATH/TO/YOUR/NEXTCLOUD";



/*****************************/
/******  BEGIN SCRIPT   ******/
/*****************************/

// Script version:
$script_version = "v2.0.0";

function check_enabled($function_name) {
  // checks, if a given function is enabled on this system
  // returns true, if the given function is enabled and returns false if it is disabled
  if ( ! is_callable ( $function_name ) ) { return false; }
  $disabled_functions = explode ( ',', ini_get ( 'disable_functions' )  );
  foreach ( $disabled_functions as $disabled_function ) {
    if ( $function_name === trim ( $disabled_function ) ) { return false; }
  }
  // function $function_name is enabled. Return true:
  return true;
}


///////////////////////////////////////////////////////////////////
// print html HEAD:
echo '<!DOCTYPE html><html lang="en">'.PHP_EOL;
echo '<head>'.PHP_EOL;
echo '<title>calcardbackup wrapped in PHP</title>'.PHP_EOL;
echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">'.PHP_EOL;
echo '<style>'.PHP_EOL;
echo 'body { font-family: sans-serif; line-height: 2em; }'.PHP_EOL;
echo 'code,pre { background-color: beige; border: 1px solid khaki; border-radius: 0.3em; padding: 0.3em; line-height: 1.2em; }'.PHP_EOL;
echo '</style>'.PHP_EOL;
echo '</head>'.PHP_EOL;

// print top of html BODY:
echo '<body>'.PHP_EOL;
echo '<h3>calcardbackup_wrapper.php '.$script_version.'</h3>'.PHP_EOL;
echo '<hr style="margin-bottom: 1.5em;">'.PHP_EOL;

// print path to this file:
echo 'path to this file: <code>'.$_SERVER['SCRIPT_FILENAME'].'</code><br>'.PHP_EOL;

// print configured command that will be invoked:
echo 'configured command: <code>'.$command.'</code><br>'.PHP_EOL;


///////////////////////////////////////////////////////////////////
// print horizontal line to seperate different sections of output:
echo '<hr style="margin: 1.5em 0em; color: #f8f8f8;">'.PHP_EOL;

echo 'Checking for enabled functions to run <i>calcardbackup</i>:<br>'.PHP_EOL;

// variable where the result will be stored until printed to the screen:
$result_checked_functions = "";
// variable to collect the enabled functions:
$enabled_functions = "";

// go through all four possible functions to execute shell scripts in order of preference and store enabled functions
// in variable $enabled_functions.
// passthru() has highest preference as it passes the output without interference to the browser window.
// shell_exec() has lowest preference, as it is the only function that does not pass the exit code of calcardbackup.
$functions = array ( 'passthru', 'system', 'exec', 'shell_exec' );
foreach ( $functions as $function ) {

  if ( check_enabled ( $function ) ) {
    $result_checked_functions .= '<nowrap><span style="color: LimeGreen; padding: 0em 0.5em;"><b>&check;</b></span><code>'.$function.'()</code> is enabled.</nowrap><br>'.PHP_EOL;
    $enabled_functions .= $function." ";
  } else {
    $result_checked_functions .= '<nowrap><span style="color: Red; padding: 0em 0.5em;"><b>&Chi;</b></span><code>'.$function.'()</code> is disabled.</nowrap><br>'.PHP_EOL;
  }

}

// print the result (all functions whith indicator whether enabled or not):
echo $result_checked_functions;


///////////////////////////////////////////////////////////////////
// print horizontal line to separate different sections of output:
echo '<hr style="margin: 1.5em 0em; color: #f8f8f8;">'.PHP_EOL;

// if $enabled_functions is still empty, there was no function found that is usable to run shell commands:
if ( $enabled_functions === "" ) {
  // there is no function enabled to run shell scripts on this system:
  echo 'To run shell scripts, either one of <code>passthru()</code>, <code>system()</code>, <code>exec()</code> or <code>shell_exec()</code> has to be enabled.<br>'.PHP_EOL;
  echo 'But all four PHP functions are disabled on this system. Thus I am not able to run <i>calcardbackup</i>.<br>Sorry.'.PHP_EOL;
  echo '</body>'.PHP_EOL.'</html>';
  exit;
}

// read the first function name from the $enabled_functions as it is the one with the highest preference we want to use to run the command:
$function_to_be_used = substr ( $enabled_functions, 0, strpos ( $enabled_functions, ' ' ) );
echo 'using <code>'.$function_to_be_used.'()</code> to run the command:<pre>'.PHP_EOL;

// run the command with the function which is set in $function_to_be_used:
switch ( $function_to_be_used ) {

  case 'passthru':
    passthru( '/usr/bin/env bash '.$command.' 2>&1', $exitcode );
    break;

  case 'system':
    system( '/usr/bin/env bash '.$command.' 2>&1', $exitcode );
    break;

  case 'exec':
    exec( '/usr/bin/env bash '.$command.' 2>&1', $output, $exitcode );
    // exec() returns an array with lines. Go through lines and print them:
    foreach ($output as $line) { echo $line . PHP_EOL; }
    break;

  case 'shell_exec':
    $output = shell_exec ( '/usr/bin/env bash '.$command.' 2>&1' );
    // print the output of the command which is stored in $output:
    echo $output;
    break;
}

// close <pre> tag:
echo '</pre>'.PHP_EOL;

// print the exit code of calcardbackup, if the calling function returned it:
if ( isset ( $exitcode ) ) {
  echo 'exitcode of <i>calcardbackup</i>: <code>'.$exitcode.'</code>'.PHP_EOL;
}

// print html footer and exit:
echo '<br><br>'.PHP_EOL.'</body>'.PHP_EOL.'</html>'.PHP_EOL;
exit;
?>
